# λx.λy. x
def TRUE(x, y):
    return x


# λx.λy. y
def FALSE(x, y):
    return y


# λb. b FALSE TRUE
def NOT(b):
    return b(FALSE, TRUE)


# λp.λq.p q p
def AND(p, q):
    return p(q, p)


# λp.λq.p p q
def OR(p, q):
    return p(p, q)


def IF_ELSE(p, a, b):
    return p(a, b)


# λf.λx.x
_0 = lambda f: lambda x: x

# λf.λx.f x
_1 = lambda f: lambda x: f(x)

# λf.λx.f (f x)
_2 = lambda f: lambda x: f(f(x))

# λf.λx.f (f (f x))
_3 = lambda f: lambda x: f(f(x))

# convert church numeral to natural
natify = lambda c: c(lambda x: x + 1)(0)


def main():
    # AND
    print(AND(FALSE, FALSE))
    print(AND(FALSE, TRUE))
    print(AND(TRUE, FALSE))
    print(AND(TRUE, TRUE))
    print("=============")
    print(OR(FALSE, FALSE))
    print(OR(FALSE, TRUE))
    print(OR(TRUE, FALSE))
    print(OR(TRUE, TRUE))
    print("=============")
    print(natify(_0))
    print(natify(_1))
    print(natify(_2))
    print("=============")
    print(IF_ELSE(TRUE, _0, _1))
    print(IF_ELSE(FALSE, _0, _1))


if __name__ == "__main__":
    main()
