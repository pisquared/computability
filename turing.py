from typing import Type


class Node(object):
    def __init__(self, next=None, prev=None):
        self.value: int = 0
        self.next: Type["Node"] = next
        self.prev: Type["Node"] = prev

    def read(self):
        return self.value

    def write(self, value):
        self.value = value


class Tape(object):
    def __init__(self):
        self.current_node = Node()

    def read(self):
        return self.current_node.read()

    def forward(self):
        if self.current_node.next is None:
            self.current_node.next = Node(prev=self.current_node)
        self.current_node = self.current_node.next

    def back(self):
        if self.current_node.prev is None:
            self.current_node.prev = Node(next=self.current_node)
        self.current_node = self.current_node.prev

    def move(self, value):
        """Helper method to move forward if value=1 or back if value=-1"""
        assert value in [1, -1]
        self.forward() if value == 1 else self.back()

    def write(self, value: int):
        assert value in [0, 1]
        self.current_node.write(value)

    def to_list(self):
        rv = []
        cur = self.current_node
        while cur.prev is not None:
            cur = cur.prev
        # we have reached the left-most node
        while cur is not None:
            rv.append(cur.value)
            cur = cur.next
        return rv


class State(object):
    def __init__(self, name, writes=None, moves=None, nexts=None, is_halt=False):
        self.name = name
        self.writes = writes or []
        self.moves = moves or []
        self.nexts = nexts or []
        self.is_halt = is_halt

    def __repr__(self):
        return "<{}> W: {} M: {} N: {}".format(self.name, self.writes, self.moves, self.nexts)


# moves
F, B = 1, -1
HALT_STATE = State("HALT", is_halt=True)


class Machine(object):
    def __init__(self, start_state: State = None):
        self.tape = Tape()
        self.current_state = start_state
        self.is_halted = False
        self.states = {}
        self.run_steps = 0

    def add_states(self, *states):
        for state in states:
            self.states[state.name] = state

    def step(self):
        if self.is_halted:
            raise Exception("Machine has halted")
        current_symbol = self.tape.read()
        write = self.current_state.writes[current_symbol]
        self.tape.write(write)
        move = self.current_state.moves[current_symbol]
        self.tape.move(move)
        next_state_name = self.current_state.nexts[current_symbol]
        next_state = self.states.get(next_state_name)
        if not next_state:
            raise Exception("State {} not defined".format(next_state_name))
        self.current_state = next_state
        if self.current_state.is_halt:
            self.is_halted = True
        self.run_steps += 1

    def run(self, max_steps=100, debug=False):
        for i in range(max_steps):
            try:
                if debug:
                    print("{}: {}".format(self.run_steps, self.current_state))
                self.step()
                if debug:
                    print("{}: {}".format(self.run_steps, self.tape.to_list()))
            except Exception:
                break


def main():
    state1 = State("A", writes=[1, 1], moves=[F, B], nexts=["B", "C"])
    state2 = State("B", writes=[1, 1], moves=[B, F], nexts=["A", "B"])
    state3 = State("C", writes=[1, 1], moves=[B, F], nexts=["B", "HALT"])

    machine = Machine(start_state=state1)
    machine.add_states(state1, state2, state3)
    machine.run(100, True)


if __name__ == "__main__":
    main()
